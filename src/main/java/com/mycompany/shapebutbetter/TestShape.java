/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapebutbetter;

/**
 *
 * @author BankMMT
 */
public class TestShape {
    public static void main(String[] args) {
        Shape shape;
        Circle circle = new Circle (5);
        System.out.printf("The area of Circle : %.2f\n",circle.calArea());
        circle.setR(0);
        System.out.printf("The area of Circle : %.2f\n",circle.calArea());
        System.out.println("--------------------");
        
        Triangle triangle = new Triangle (5,10);
        System.out.printf("The area of Triangle : %.2f\n",triangle.calArea());
        triangle.setBase(0);
        triangle.setHeight(0);
        System.out.printf("The area of Triangle : %.2f\n",triangle.calArea());
        System.out.println("--------------------");
        
        Recangle recangle = new Recangle (5,10);
        System.out.printf("The area of Recangle : %.2f\n",recangle.calArea());
        recangle.setWidth(0);
        recangle.setLength(0);
        System.out.printf("The area of Recangle : %.2f\n",recangle.calArea());
        System.out.println("--------------------");
        
        Square square = new Square (5);
        System.out.printf("The area of Square : %.2f\n",square.calArea());
        square.setEdge(0);
        System.out.printf("The area of Square : %.2f\n",square.calArea());
        System.out.println("--------------------");
        
        Shape[] shapeButLoop = {triangle,circle,recangle,square};
        for (int i=0 ; i<shapeButLoop.length ; i++){
            System.out.printf("%.2f \n--------------------\n",shapeButLoop[i].calArea());
        }
    }
}
