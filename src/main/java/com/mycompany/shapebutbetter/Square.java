/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapebutbetter;

/**
 *
 * @author BankMMT
 */
public class Square extends Recangle{
    protected double edge;                   
    public Square (double edge){
        super(edge,edge);
        this.edge = edge;
    }
    @Override
    public double calArea(){
        double sum  = edge*edge;                         //sum = summarry
        return sum;
    }public double getEdge(){
        return edge;
    }public void setEdge(double edge){
        if (edge <= 0){
            System.out.println("Error: Edge must more than zero !!!");
            return;
        }this.edge = edge;
    }
}
