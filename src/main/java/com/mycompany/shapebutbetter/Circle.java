/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapebutbetter;

/**
 *
 * @author BankMMT
 */
public class Circle extends Shape{
    protected double r;
    protected double pi = 22.0/7.0;
    
    public Circle (double r){
        super ();
        this.r = r;
        this.pi = pi;
        
    }
    
    @Override
    public double calArea(){
        double sum = (r*r)*pi;                  // sum = summary
        return sum;
    }
    public double getR (){
        return r;
    }
    public void setR (double r){
        if (r <= 0){
            System.out.println("Error : Radius must more than zero!!!");
            return;
        }this.r = r;
    }
}
