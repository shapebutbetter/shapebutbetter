/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapebutbetter;

/**
 *
 * @author BankMMT
 */
public class Triangle extends Shape{
    protected double base;
    protected double height;
    public Triangle (double base, double height){
        super();
        this.base = base;
        this.height = height;
    }
    @Override
    public double calArea(){
        double sum = base*height*0.5;                   // sum = summary
        return sum;
    }
    public double getBase(){
        return base;
    }
    public double getHeight(){
        return height;
    }
    public void setBase(double base) {
        if (base <= 0){
            System.out.println("Error : Base must more than zero!!!");
            return;
        }this.base = base;
    }
    public void setHeight(double height){
        if (height <= 0){
            System.out.println("Error : Height must more than zero!!!");
            return;
        }this.height = height;
    }
}
