/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapebutbetter;

/**
 *
 * @author BankMMT
 */
public class Recangle extends Shape{
    protected double width;
    protected double length;
    public Recangle (double width, double length){
        super();
        this.width = width;
        this.length = length;
    }
    @Override
    public double calArea(){
        double sum = width*length;                     // sum = sumarry
        return sum;
    }
    public double getWidth(){
        return width;
    }
    public double getLength(){
        return length;
    }
    public void setWidth(double width){
        if (width <= 0){
            System.out.println("Error : Width must more than zero!!!");
            return;
        }this.width = width;
    }
    public void setLength(double length){
        if (length <= 0){
            System.out.println("Error : Length must more than zero!!!");
            return;
        }this.length = length;
    }
}
